-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 19 Avril 2017 à 19:54
-- Version du serveur :  5.7.17-0ubuntu0.16.04.2
-- Version de PHP :  5.6.30-10+deb.sury.org~xenial+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `avsii`
--

-- --------------------------------------------------------

--
-- Structure de la table `Miniature`
--

CREATE TABLE `Miniature` (
  `idMiniature` int(11) NOT NULL,
  `nomMiniature` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `Offre`
--

CREATE TABLE `Offre` (
  `idOffre` int(11) NOT NULL,
  `nomOffre` varchar(127) NOT NULL,
  `dureeLimiteOffre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Offre`
--

INSERT INTO `Offre` (`idOffre`, `nomOffre`, `dureeLimiteOffre`) VALUES
(1, 'Standard', 3600),
(2, 'Premium', 86400);

-- --------------------------------------------------------

--
-- Structure de la table `Utilisateur`
--

CREATE TABLE `Utilisateur` (
  `idUtilisateur` int(11) NOT NULL,
  `mailUtilisateur` varchar(127) NOT NULL,
  `passUtilisateur` varchar(255) NOT NULL,
  `dateSouscription` datetime NOT NULL,
  `idOffre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Utilisateur`
--

INSERT INTO `Utilisateur` (`idUtilisateur`, `mailUtilisateur`, `passUtilisateur`, `dateSouscription`, `idOffre`) VALUES
(2, 'adam.macheda74@gmail.com', 'aa36dc6e81e2ac7ad03e12fedcb6a2c0', '2017-04-19 00:00:00', 1);

-- --------------------------------------------------------

--
-- Structure de la table `Video`
--

CREATE TABLE `Video` (
  `idVideo` int(11) NOT NULL,
  `dureeVideo` int(11) NOT NULL,
  `dateVideo` date NOT NULL,
  `heureVideo` time NOT NULL,
  `nomVideo` varchar(255) NOT NULL,
  `idUtilisateur` int(11) NOT NULL,
  `idMiniature` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Miniature`
--
ALTER TABLE `Miniature`
  ADD PRIMARY KEY (`idMiniature`);

--
-- Index pour la table `Offre`
--
ALTER TABLE `Offre`
  ADD PRIMARY KEY (`idOffre`);

--
-- Index pour la table `Utilisateur`
--
ALTER TABLE `Utilisateur`
  ADD PRIMARY KEY (`idUtilisateur`),
  ADD KEY `FK_Utilisateur_idOffre` (`idOffre`);

--
-- Index pour la table `Video`
--
ALTER TABLE `Video`
  ADD PRIMARY KEY (`idVideo`),
  ADD KEY `FK_Video_idUtilisateur` (`idUtilisateur`),
  ADD KEY `FK_Video_idMiniature` (`idMiniature`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Miniature`
--
ALTER TABLE `Miniature`
  MODIFY `idMiniature` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `Offre`
--
ALTER TABLE `Offre`
  MODIFY `idOffre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `Utilisateur`
--
ALTER TABLE `Utilisateur`
  MODIFY `idUtilisateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `Video`
--
ALTER TABLE `Video`
  MODIFY `idVideo` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Utilisateur`
--
ALTER TABLE `Utilisateur`
  ADD CONSTRAINT `FK_Utilisateur_idOffre` FOREIGN KEY (`idOffre`) REFERENCES `Offre` (`idOffre`);

--
-- Contraintes pour la table `Video`
--
ALTER TABLE `Video`
  ADD CONSTRAINT `FK_Video_idMiniature` FOREIGN KEY (`idMiniature`) REFERENCES `Miniature` (`idMiniature`),
  ADD CONSTRAINT `FK_Video_idUtilisateur` FOREIGN KEY (`idUtilisateur`) REFERENCES `Utilisateur` (`idUtilisateur`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
