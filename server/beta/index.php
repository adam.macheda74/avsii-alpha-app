<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);

header("Access-Control-Allow-Origin: *");


include_once "config.php";
include_once "db.php";
session_start();
require_once "vendor/autoload.php";
include_once "controller/controller.php";
