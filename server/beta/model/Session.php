<?php
class Session {
  protected $utilisateurSession;
  protected $tokenSession;
  protected $statutSession;

  public function __construct($utilisateur) {
    if(!isset($_SESSION['s_statutSession'])) {
      $this->utilisateurSession = $utilisateur;
      $this->tokenSession = md5(rand());
      $this->statutSession = "Started";

      $_SESSION['s_utilisateurSession'] = $this->utilisateurSession;
      $_SESSION['s_tokenSession'] = $this->tokenSession;
      $_SESSION['s_statutSession'] = $this->statutSession;
    } else {
      $this->utilisateurSession = $_SESSION['s_utilisateurSession'];
      $this->tokenSession = $_SESSION['s_tokenSession'];
      $this->statutSession = $_SESSION['s_statutSession'];
    }
  }

  public function __get($fieldname) {
		return $this->$fieldname;
	}

	public function __set($fieldname, $value) {
		$this->$fieldname = $value;
    $_SESSION['s_'.$fieldname] = $value;
	}

  public static function killSession() {
    unset($_SESSION['s_utilisateurSession']);
    unset($_SESSION['s_tokenSession']);
    unset($_SESSION['s_statutSession']);

    return true;
  }

  public static function getSession() {
     if(isset($_SESSION['s_statutSession'])) {
       $s = new Session(null);
       return $s;
     } else {
       return false;
     }
   }
}
