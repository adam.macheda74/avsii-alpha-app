<?php
class Usual {
  public static function startsWith($haystack, $needle) {
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
  }

  public static function endsWith($haystack, $needle) {
    $length = strlen($needle);
    if ($length == 0) {
      return true;
    }
    return (substr($haystack, -$length) === $needle);
  }

  public static function timeToSeconds($time) {
    sscanf($time, "%d:%d", $minutes, $seconds);

    $time_seconds = isset($seconds) ? $minutes * 60 + $seconds : $hours * 60 + $minutes;

    return $time_seconds;
  }

  public static function validateDate($date, $format = 'Y-m-d H:i:s') {
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
  }

}
