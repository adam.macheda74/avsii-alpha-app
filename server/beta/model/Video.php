<?php
class Video extends Model {
	protected $idVideo;
	protected $dureeVideo;
	protected $dateVideo;
	protected $heureVideo;
	protected $nomVideo;
	protected $idUtilisateur;
	protected $idMiniature;

	public static function getVideoListByUser($user) {
		$nomUtilisateur = $user->idUtilisateur;
		$videoListe = array();
		$videoListe["miniIndispo"] = array();
		$videoListe["listeVideos"] = array();
		$videoListe["nbVideosByDate"] = array();

		chdir("../uploads/");

		$fichiers = glob($nomUtilisateur."_*.mp4");
		usort($fichiers, function($a,$b){
			return filemtime($b) - filemtime($a);
		});

		foreach ($fichiers as $filename) {
				if(!file_exists("thumbs/".$filename.".jpg")) {
					array_push($videoListe["miniIndispo"], $filename);
				}

				$videoListe["listeVideos"][] = array($filename, "thumbs/".$filename.".jpg", date("d/m/Y",filemtime($filename)), date("H",filemtime($filename))."h".date("i",filemtime($filename)), substr(shell_exec("ffmpeg -i /var/www/html/uploads/$filename 2>&1 | grep Duration | awk '{print $2}' | tr -d ,"), 3, 5));
		}

		foreach ($fichiers as $filename) {
			if(array_key_exists(date("d/m/Y",filemtime($filename)), $videoListe["nbVideosByDate"])) {
				if(substr(shell_exec("ffmpeg -i /var/www/html/uploads/$filename 2>&1 | grep Duration | awk '{print $2}' | tr -d ,"), 3, 5) != false) {
					$videoListe["nbVideosByDate"][date("d/m/Y",filemtime($filename))] += 1;
				}
			} else {
				if(substr(shell_exec("ffmpeg -i /var/www/html/uploads/$filename 2>&1 | grep Duration | awk '{print $2}' | tr -d ,"), 3, 5) != false) {
					$videoListe["nbVideosByDate"][date("d/m/Y",filemtime($filename))] = 1;
				}
			}
		}

		chdir("../beta");
		return $videoListe;
	}

	public static function deleteVideoByName($user, $name) {
		$nomUtilisateur = $user->idUtilisateur;

		if(Usual::startsWith($name, $nomUtilisateur."_") && Usual::endsWith($name, ".mp4")) {
			if(unlink("../uploads/".$name)) {
				return true;
			} else {
				return false;
			}
		} else return false;
	}

	public static function addVideo($file) {
		$dirname = "../uploads/";
		if(!file_exists($dirname))
			mkdir($dirname, 0777, true);

		if(!file_exists($dirname."thumbs/"))
			mkdir($dirname."thumbs/", 0777, true);

		if($file) {
			if(strtolower(end(explode(".",$file["file"]["name"]))) == "mp4") {
				mkdir ($dirname, 0777, true);
				move_uploaded_file($file["file"]["tmp_name"],$dirname."/".Session::getSession()->utilisateurSession->idUtilisateur."_".$file["file"]["name"]);

				$video = '/var/www/html/uploads/'.Session::getSession()->utilisateurSession->idUtilisateur."_".$file["file"]["name"];
				$thumbnail = '/var/www/html/uploads/thumbs/'.Session::getSession()->utilisateurSession->idUtilisateur."_".$file["file"]["name"].".jpg";

				// shell command [highly simplified, please don't run it plain on your script!]
				echo shell_exec("ffmpeg -i $video -deinterlace -an -ss 1 -t 00:00:01 -r 1 -y -vcodec mjpeg -f mjpeg $thumbnail 2>&1");
				return true;
			} else return false;
		} else return false;
	}

	public static function getStockageConsomme($liste) {
		$totalSecondes = 0;

		foreach($liste['listeVideos'] as $uneVideo) {
			$totalSecondes += Usual::timeToSeconds($uneVideo[4]);
		}

		return $totalSecondes;
	}

}
