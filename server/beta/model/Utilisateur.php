<?php
class Utilisateur extends Model {
	protected $idUtilisateur;
	protected $fbIdUtilisateur;
	protected $mailUtilisateur;
	protected $passUtilisateur;
	protected $dateSouscription;
	protected $idOffre;

	public function checkPassword($mdp) {
		if(md5($mdp) == $this->passUtilisateur)
			return true;
		else
			return false;
	}

	public static function existsByMail($mail) {
		global $db;

		$st = $db->prepare("SELECT COUNT(idUtilisateur) FROM Utilisateur
												WHERE mailUtilisateur=:mail");
		$st->bindValue(":mail", $mail);
		$st->execute();
		$num_rows = $st->fetchColumn();

		if($num_rows > 0)
			return true;
		else
			return false;
	}

	public static function existsByFbId($id) {
		global $db;

		$st = $db->prepare("SELECT COUNT(idUtilisateur) FROM Utilisateur
												WHERE fbIdUtilisateur=:id");
		$st->bindValue(":id", $id);
		$st->execute();
		$num_rows = $st->fetchColumn();

		if($num_rows > 0)
			return true;
		else
			return false;
	}

	public static function getByMail($mail) {
		global $db;

		$st = $db->prepare("SELECT idUtilisateur FROM Utilisateur
												WHERE mailUtilisateur=:mail");
		$st->bindValue(":mail", $mail);
		$st->execute();
		$row = $st->fetch();

		return new Utilisateur($row['idUtilisateur']);
	}

	public static function getByFbId($id) {
		global $db;

		$st = $db->prepare("SELECT idUtilisateur FROM Utilisateur
												WHERE fbIdUtilisateur=:id");
		$st->bindValue(":id", $id);
		$st->execute();
		$row = $st->fetch();

		return new Utilisateur($row['idUtilisateur']);
	}

	public static function createNew($mail, $mdp) {
		global $db, $_CONFIG;

		$st = $db->prepare("INSERT INTO Utilisateur (mailUtilisateur, passUtilisateur, dateSouscription, idOffre)
												VALUES (:mail, :pass, CURRENT_TIMESTAMP, :id)");
		$st->bindValue(":mail", $mail);
		$st->bindValue(":pass", md5($mdp));
		$st->bindValue(":id", $_CONFIG['offre_defaut']);
		$st->execute();

		return new Utilisateur($db->lastInsertId());
	}

	public static function createByFb($mail, $fbId) {
		global $db, $_CONFIG;

		$st = $db->prepare("INSERT INTO Utilisateur (mailUtilisateur, passUtilisateur, dateSouscription, idOffre, fbIdUtilisateur)
												VALUES (:mail, :pass, CURRENT_TIMESTAMP, :id, :fbId)");
		$st->bindValue(":mail", $mail);
		$st->bindValue(":fbId", $fbId);
		$st->bindValue(":pass", md5(uniqid()));
		$st->bindValue(":id", $_CONFIG['offre_defaut']);
		$st->execute();

		return new Utilisateur($db->lastInsertId());
	}

	public static function getStockageConsomme($liste) {
		$totalSecondes = 0;

		foreach($liste['listeVideos'] as $uneVideo) {
			$totalSecondes += Usual::timeToSeconds($uneVideo[4]);
		}

		return $totalSecondes;
	}

	public static function getStockageRestant($liste) {
		$offre_duree = 60;

		return gmdate("H:i:s", ($offre_duree*60)-Utilisateur::getStockageConsomme($liste));
	}

	public static function getPourcentageStockage($liste) {
		$offre_duree = 60;

		return round((Utilisateur::getStockageConsomme($liste)/($offre_duree*60))*100);
	}

}
