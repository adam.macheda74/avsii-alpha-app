<?php
class Model {
	public function __construct($id=null) {
		global $db;

		$class = get_class($this);

		if ($id != null) {
			$st = $db->prepare("SELECT * FROM $class
													WHERE id$class=:id");
			$st->bindValue(":id", $id);
			$st->execute();
			$row = $st->fetch(PDO::FETCH_ASSOC);

			foreach($row as $attr => $value)
				$this->$attr = $value;
		}
	}

	public function __get($fieldname) {
		return $this->$fieldname;
	}

	public function __set($fieldname, $value) {
		global $db;

		$this->$fieldname = $value;
		$class = get_class($this);
		$st = $db->prepare("update $class set
			$fieldname=:value where id$class=:id");
		$id = "id".$class;
		$st->bindValue(":id",$this->$id);
		$st->bindValue(":value",$value);
		$st->execute();
	}

	public function __toString() {
		$s = "<div>";
		$s .= "<h2>".get_class($this). "</h2><ul>";
		foreach(get_object_vars($this) as $attr=>$value) {
			$s .= "<li>".$attr." = ".$value."</li>";

		}
		$s .= "</ul></div>";
		return $s;
	}

	public static function findAll() {
		global $db;

		$class = get_called_class();
		$table = $class;

		$st = $db->prepare("SELECT * FROM $table");
		$st->execute();

		$stuffList = array();
		while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
			$stuffList[] = new $class($row["id".$table]);
		}

		return $stuffList;
	}

	public static function exists($id) {
		global $db;

		$class = get_called_class();
		$table = $class;

		$st = $db->prepare("SELECT COUNT(id$class) FROM $class
												WHERE id$class=:id");
		$st->bindValue(":id", $id);
		$st->execute();
		$num_rows = $st->fetchColumn();

		if($num_rows > 0)
			return true;
		else
			return false;
	}
}
