<?php
// ROUTE
// GET : ?r=model/action
// Exemple : ?r=movie/list

$data = "";

function render($view) {
	global $data;
	$data['viewMode'] = 'home';
	include_once "view/header.php";
	include_once "view/".$view.".php";
	include_once "view/footer.php";
}

function renderMembre($view) {
	global $data;
	$data['viewMode'] = 'admin';
	include_once "view/header.php";
	include_once "view/".$view.".php";
	include_once "view/footer.php";
}

function renderJson() {
	global $data;
	include_once "view/json.php";
}

if(!isset($_GET["r"]) || $_GET["r"] == "") {
	header('Location: ./page/accueil');
}

$route = explode("/", strtolower($_GET["r"]));
$data['route'] = $route;

if($route[0] == "page") {
	if(isset($route[1]) && $route[1] == "accueil") {
		render("accueil");
	} else if(isset($route[1]) && $route[1] == "membre") {
		if(Session::getSession() != false) {
			if(isset($route[2]) && $route[2] == "enregistrements") {
				if(isset($route[3]) && $route[3] == "session" && isset($_POST['id'])) {
					if(Usual::validateDate('24/05/2017', 'd/m/Y')) {
						$data['id'] = $_POST['id'];
						$data['listeVideos'] = Video::getVideoListByUser(Session::getSession()->utilisateurSession);
						$data['page'] = 0;
						$data['titre'] = "Enregistrements et informations";
						$data['description'] = "Visualiser les enregistrements de la session du <b>".$data['id']."</b>.";
						renderMembre("session");
					} else {
						$ret = "";
						for($i = 0; $i<count($data['route'])-1; $i++) $ret .= '../';

						header('Location: '.$ret.'page/membre/enregistrements');
					}
				} else {
					$data['listeVideos'] = Video::getVideoListByUser(Session::getSession()->utilisateurSession);
					$data['page'] = 0;
					$data['titre'] = "Enregistrements et informations";
					$data['description'] = "Consulter la liste des preuves enregistrées ainsi que les informations de stockage.";
					renderMembre("enregistrements");
				}
			} else if(isset($route[2]) && $route[2] == "mdp") {
				$data['listeVideos'] = Video::getVideoListByUser(Session::getSession()->utilisateurSession);
				$data['page'] = 1;
				$data['titre'] = "Gestion du mot de passe";
				$data['description'] = "Choisir un nouveau mot de passe ou le réinitialiser.";
				renderMembre("mdp");
			} else if(isset($route[2]) && $route[2] == "offre") {
				$data['listeVideos'] = Video::getVideoListByUser(Session::getSession()->utilisateurSession);
				$data['page'] = 2;
				$data['titre'] = "Abonnements et offres";
				$data['description'] = "Souscrire à une offre ou se désabonner.";
				renderMembre("enregistrements");
			} else if(isset($route[2]) && $route[2] == "facebook") {
				$data['listeVideos'] = Video::getVideoListByUser(Session::getSession()->utilisateurSession);
				$data['page'] = 3;
				$data['titre'] = "Gérer mon compte facebook";
				$data['description'] = "Lier un compte facebook à un compte AVSSI pour une connexion plus rapide.";
				renderMembre("enregistrements");
			} else {
				if(Session::getSession() == false) {
					renderMembre("login");
				} else {
					$ret = "";
					for($i = 0; $i<count($data['route'])-1; $i++) $ret .= '../';

					header('Location: '.$ret.'page/membre/enregistrements');
				}
			}
		} else {
			renderMembre("login");
		}
	} else {
		render("404");
	}


} else if($route[0] == "service") {
	if(isset($route[1]) && $route[1] == "connexion") {
		if(isset($_POST['mail']) && isset($_POST['pass'])) {
			if(Utilisateur::existsByMail(strtolower($_POST['mail']))) {
				$user = Utilisateur::getByMail(strtolower($_POST['mail']));
				if($user->checkPassword($_POST['pass'])) {
					if(Session::getSession() != false)
				  	Session::killSession();
					$data['result'] = true;
					$session = new Session($user);
				} else $data['result'] = false;
			} else $data['result'] = false;
		} else $data['result'] = false;

		renderJson();
	} else if(isset($route[1]) && $route[1] == "mdp") {
		if(isset($_POST['newpass']) && isset($_POST['oldpass'])) {
			if(Session::getSession() != false) {
				if(Session::getSession()->utilisateurSession->passUtilisateur == md5($_POST['oldpass'])) {
					if(strlen(trim($_POST['newpass'])) >= 6) {
						Session::getSession()->utilisateurSession->passUtilisateur = md5($_POST['newpass']);
						$data['result'] = true;
					} else { $data['result'] = false; $data['error'] = "Le mot de passe doit faire au moins 6 caract&egrave;res."; }
				} else { $data['result'] = false; $data['error'] = "Votre ancien mot de passe est incorrect."; }
			} else { $data['result'] = false; $data['error'] = "Vous devez &ecirc;tre connect&eacute;."; }
		} else { $data['result'] = false; $data['error'] = "Une erreur est survenue lors de l'envoi des donn&eacute;es."; }

		renderJson();
	} else if(isset($route[1]) && $route[1] == "deconnexion") {
		$data['result'] = Session::killSession();

		renderJson();

		if(isset($_GET['redirect']) && $_GET['redirect'])
			Header('Location: ../page/membre');
	} else if(isset($route[1]) && $route[1] == "inscription") {
		if(isset($_POST['mail']) && isset($_POST['pass']) && isset($_POST['confirm'])) {
			if(!filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL) === false) {
				if(!Utilisateur::existsByMail(strtolower($_POST['mail']))) {
					if($_POST['pass'] == $_POST['confirm']) {
						if(strlen(trim($_POST['pass'])) >= 6) {
							$user = Utilisateur::createNew(strtolower($_POST['mail']), $_POST['pass']);
							$session = new Session($user);
							$data['result'] = true;
						} else { $data['result'] = false; $data['error'] = "Le mot de passe doit faire au moins 6 caract&egrave;res."; }
					} else { $data['result'] = false; $data['error'] = "Les mots de passe ne correspondent pas !"; }
				} else { $data['result'] = false; $data['error'] = "L'adresse mail existe d&eacutej&agrave; !"; }
			} else { $data['result'] = false; $data['error'] = "L'adresse mail a un format incorrect !"; }
		}

		renderJson();
	} else if(isset($route[1]) && $route[1] == "session") {
		if(Session::getSession() != false) {
			$data['utilisateur'] = array(
				'idUtilisateur' => Session::getSession()->utilisateurSession->idUtilisateur,
				'mailUtilisateur' => Session::getSession()->utilisateurSession->mailUtilisateur,
				'idOffre' => Session::getSession()->utilisateurSession->idOffre,
				'dateSouscription' => Session::getSession()->utilisateurSession->dateSouscription
			);

			$data['token'] = Session::getSession()->tokenSession;

			$data['statut'] = Session::getSession()->statutSession;
		} else $data['statut'] = false;

		renderJson();
	} else {
		render("404");
	}


} else if($route[0] == "facebook") {
	if(isset($route[1]) && $route[1] == "login") {
		$fb = new Facebook\Facebook([
		  'app_id' => '676909922492109', // Replace {app-id} with your app id
		  'app_secret' => 'a585ea35f31a528dd7b628b18756e297',
		  'default_graph_version' => 'v2.9',
		  ]);

		$helper = $fb->getRedirectLoginHelper();

		$permissions = ['email'];
		$loginUrl = $helper->getLoginUrl('http://34.209.142.173/beta/index.php?r=facebook/callback', $permissions);

	  $url = htmlspecialchars($loginUrl);
	  header("Location: $loginUrl");
	} else if(isset($route[1]) && $route[1] == "callback") {
		$fb = new Facebook\Facebook([
			'app_id' => '676909922492109', // Replace {app-id} with your app id
			'app_secret' => 'a585ea35f31a528dd7b628b18756e297',
			'default_graph_version' => 'v2.9',
			]);

		$helper = $fb->getRedirectLoginHelper();

		if(!isset($_SESSION['fb_access_token'])) {
			try {
		    $accessToken = $helper->getAccessToken();
		  } catch(Facebook\Exceptions\FacebookResponseException $e) {
		    // When Graph returns an error
		    echo 'Graph returned an error: ' . $e->getMessage();
		    exit;
		  } catch(Facebook\Exceptions\FacebookSDKException $e) {
		    // When validation fails or other local issues
		    echo 'Facebook SDK returned an error: ' . $e->getMessage();
		    exit;
		  }
		} else {
			$accessToken = $_SESSION['fb_access_token'];
		}

	  if (! isset($accessToken)) {
	    if ($helper->getError()) {
	      header('HTTP/1.0 401 Unauthorized');
	      echo "Error: " . $helper->getError() . "\n";
	      echo "Error Code: " . $helper->getErrorCode() . "\n";
	      echo "Error Reason: " . $helper->getErrorReason() . "\n";
	      echo "Error Description: " . $helper->getErrorDescription() . "\n";
	    } else {
	      header('HTTP/1.0 400 Bad Request');
	      echo 'Bad request';
	    }
	    exit;
	  }

	  // // Logged in
	  // echo '<h3>Access Token</h3>';
	  // var_dump($accessToken->getValue());
		//
	  // // The OAuth 2.0 client handler helps us manage access tokens
	  // $oAuth2Client = $fb->getOAuth2Client();
		//
	  // // Get the access token metadata from /debug_token
	  // $tokenMetadata = $oAuth2Client->debugToken($accessToken);
	  // echo '<h3>Metadata</h3>';


		// The OAuth 2.0 client handler helps us manage access tokens
		$oAuth2Client = $fb->getOAuth2Client();

		// Get the access token metadata from /debug_token
		$tokenMetadata = $oAuth2Client->debugToken($accessToken);

		if(Utilisateur::existsByFbId($tokenMetadata->getUserId())) {
			$user = Utilisateur::getByFbId($tokenMetadata->getUserId());
			if(Session::getSession() != false)
				Session::killSession();

			$session = new Session($user);
		} else {
			try {
					$response = $fb->get('/me?fields=email', $accessToken);
			} catch(\Facebook\Exceptions\FacebookResponseException $e) {
					echo $e->getMessage();
					exit();
			} catch(\Facebook\Exceptions\FacebookSDKException $e) {
					// When validation fails or other local issues
					echo $e->getMessage();
					exit();
			}

			// Get user details from facebook.
			$me = $response->getGraphUser();

			if(Utilisateur::existsByMail($me['email'])) {
				$user = Utilisateur::getByMail($me['email']);
				$user->fbIdUtilisateur = $tokenMetadata->getUserId();

				if(Session::getSession() != false)
					Session::killSession();

				$session = new Session($user);
			} else {
				$user = Utilisateur::createByFb(strtolower($me['email']), $tokenMetadata->getUserId());

				if(Session::getSession() != false)
					Session::killSession();

				$session = new Session($user);
			}
		}
		$_SESSION['fb_access_token'] = $accessToken;

		$ret = "";
		for($i = 0; $i<count($data['route'])-1; $i++) $ret .= '../';

		header('Location: http://34.209.142.173/beta/page/membre/enregistrements&fbsuccess');
	}


} else if ($route[0] == "video") {
	if(isset($route[1]) && $route[1] == "upload") {
		if(Session::getSession() != false) {
			$data['result'] = Video::addVideo($_FILES);
		} else $data['result'] = false;

		renderJson();
	} else if(isset($route[1]) && $route[1] == "liste") {
		if(Session::getSession() != false) {
			$data = Video::getVideoListByUser(Session::getSession()->utilisateurSession);
		} else $data['result'] = false;

		renderJson();
	} else if(isset($route[1]) && $route[1] == "delete") {
		if(Session::getSession() != false) {
			if(isset($_POST['vid'])) {
		    $data['result'] = Video::deleteVideoByName(Session::getSession()->utilisateurSession, $_POST['vid']);
		  }
		} else $data['result'] = false;

		renderJson();
	} else {
		render("404");
	}
} else {
	render("404");
}
