<section class="login-form">
  <div class="log-container">
    <div class="flex-container">
      <a class="back-button"href="<?php for($i = 0; $i<count($data['route'])-1; $i++) echo('../'); ?>page/accueil"><i class="fa fa-arrow-left"></i></a>

      <div class="loader"></div>
    </div>

    <h1>Membre</h1>
    <h2>Accès à vos preuves enregistrées</h2>

    <input type="text" id="mailInput" onkeypress="if (event.keyCode==13){ connexion(); return false;}" placeholder="Adresse email" />

    <input type="password" id="passInput" onkeypress="if (event.keyCode==13){ connexion(); return false;}" placeholder="Mot de passe" />

    <div class="login-box-container">
      <div class="flex-container">
        <input type="submit" onclick="connexion();" value="Connexion" />

        <a href="<?php for($i = 0; $i<count($data['route'])-1; $i++) echo('../'); ?>facebook/login"><i class="fa fa-facebook"></i></a>
      </div>
    </div>

    <div class="footer">
      <a href="#" onclick="mdpOublie();">Mot de passe oublié ?</a>
    </div>
  </div>

  <div class="mdp-container">
    <div class="flex-container">
      <a class="back-button" href="#" onclick="backLogin();"><i class="fa fa-arrow-left"></i></a>

      <div class="loader"></div>
    </div>

    <h2>Mot de passe oublié ?</h2>

    <input type="text" id="mailMdpInput" placeholder="Adresse email" />

    <input type="submit" value="Me renvoyer un mot de passe" />

  </div>
</section>
