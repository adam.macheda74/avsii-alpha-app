    <?php if($data['viewMode'] != 'admin') { ?>

    <footer>
      <div class="center-block">
        <div class="flex-container">
          <p>Copyright &copy; <?php echo date('Y'); ?> AVSSI. Tous droits réservés | Conception graphique par <a href="http://www.adammacheda.com/">Adam Macheda</a></p>
          <ul>
            <li><a href="#">Mentions légales</a></li>
            <li>&nbsp;-&nbsp;</li>
            <li><a href="#">Conditions d'utilisation</a></li>
          </ul>
        </div>
      </div>
    </footer>

    <?php } else { echo'<div id="centre-notifications"></div>';
      if(isset($data['page'])) echo '</nav>'; } ?>

    <script type="text/javascript" src="<?php for($i = 0; $i<count($data['route']); $i++) echo('../'); ?>res/lib/jquery-3.2.0.min.js"></script>
    <script type="text/javascript" src="<?php for($i = 0; $i<count($data['route']); $i++) echo('../'); ?>res/js/notification.js"></script>
    <script type="text/javascript" src="<?php for($i = 0; $i<count($data['route']); $i++) echo('../'); ?>res/js/accueil.js"></script>
    <script type="text/javascript" src="<?php for($i = 0; $i<count($data['route']); $i++) echo('../'); ?>res/js/login.js"></script>
    <script type="text/javascript" src="<?php for($i = 0; $i<count($data['route']); $i++) echo('../'); ?>res/js/membre.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCP8cuseGAksJagmVIM7FwzYUHTOqXsHCE&callback=initMap"></script>
  </body>
</html>
