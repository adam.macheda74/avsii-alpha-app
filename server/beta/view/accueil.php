<section class="services">
  <div class="center-block">
    <h1>Notre service</h1>
    <h2>La vidéo surveillance mobile, partout.</h2>

    <div class="services-list">
      <div class="flex-container">
        <div class="un-service">
          <div class="service-security"></div>
          <p>Sécurité</p>
          <span>AVSSI vous offre la chance de vous sentir en sécurité partout où vous allez, avec la certitude que quoi qu'il arrive, nous serrons à vos côtés.</span>
        </div>

        <div class="un-service">
          <div class="service-reliability"></div>
          <p>Fiabilité</p>
          <span>Nous privilégions la qualité et la fiabilité du matériel et des serveurs que nous utilisons afin de garantir à nos utilisateurs le meilleur, le plus fiable sur le marché.</span>
        </div>

        <div class="un-service">
          <div class="service-accessibility"></div>
          <p>Accessibilité</p>
          <span>Nous offrons à nos utilisateurs la possiblité de visualiser leurs enregistrements vidéo à tout moment et depuis n'importe où dans le monde, via notre site internet.</span>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="offres">
  <div class="center-block">
    <h1>Nos offres</h1>
    <h2>Transparentes, pas de frais cachés.</h2>

    <div class="offres-list">
      <div class="flex-container">
        <div class="une-offre">
          <h3>Standard</h3>
          <h4>Gratuit</h4>

          <p class="first"><i></i> 1 heure d'enregistrement</p>
          <p><i></i> Consultation en ligne</p>
          <p><i></i>  Accès en téléchargement</p>
          <p><i></i>  Connexion par facebook</p>
          <p><i></i>  Suppression des vidéos</p>

          <a href="#"><h5>Télécharger</h5></a>
        </div>

        <div class="une-offre">
          <h3>Premium</h3>
          <h4>2,99€ <span>TTC</span></h4>

          <p class="first"><i></i>  24h d'enregistrement</p>
          <p><i></i>  Consultation en ligne</p>
          <p><i></i>  Accès en téléchargement</p>
          <p><i></i>  Connexion par facebook</p>
          <p><i></i>  Publication sur Youtube</p>

          <a href="#"><h5>Acheter</h5></a>
        </div>

        <div class="une-offre">
          <h3>Prestige</h3>
          <h4>9,99€ <span>TTC</span></h4>

          <p class="first"><i></i>  Temps illimité</p>
          <p><i></i>  SAV 24h/24 7j/7</p>
          <p><i></i>  Consultation en ligne</p>
          <p><i></i>  Accès en téléchargement</p>
          <p><i></i>  Connexion par facebook</p>

          <a href="#"><h5>Acheter</h5></a>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="fonctionnement">
  <div class="center-block">
    <h1>Fonctionnement</h1>
    <h2>La clé : la simplicité, l'intuitivité.</h2>

    <div class="viewer-container">
      <div class="phone-viewer">
        <div id="img-1"></div>
        <div id="img-2"></div>
        <div id="img-3"></div>
      </div>
    </div>
    <div class="phone-description">
      <div class="une-desc" id="dec-1" style="opacity:1;">
        <h3>Connexion</h3>
        Le design épuré de l'application permet de cerner très rapidement son fonctionnement : commencez par vous connecter ou par créer un compte. Vous pouvez aussi utiliser votre compte Facebook pour vous connecter à l'application.
      </div>

      <div class="une-desc" id="dec-2" style="display:none;">
        <h3>Visualiser</h3>
        Une fois connecté, vous pouvez visionner à volonté les vidéo-surveillances dans la liste de vos enregistrements. L'application est intelligente et les classe automatiquement par ordre chronologique pour faciliter la navigation.
      </div>

      <div class="une-desc" id="dec-3" style="display:none;">
        <h3>Surveillance</h3>
        Pour lancer une nouvelle surveillance vidéo, accèdez à l'interface d'enregistrement en touchant l'icône représentant une caméra accompagnée du sigle +, dans la partie supérieure gauche de l'écran précédant. Démarrez la surveillance.
      </div>

      <div class="nav-boutons">
        <div class="bouton" onclick="setSlide(1);" id="bt-1"><div class="plein"></div></div>
        <div class="bouton" onclick="setSlide(2);" id="bt-2"><div class="plein" style="opacity:0;"></div></div>
        <div class="bouton" onclick="setSlide(3);" id="bt-3"><div class="plein" style="opacity:0;"></div></div>
      </div>
  </div>
</section>


<section class="contact">
  <div class="center-block">
    <h2>Restez en contact avec nos équipes</h2>
    <h1><span>+33</span> 659 - 919 - 008</h1>

    <div class="bottom-container flex-container">
      <div class="une-colonne">
        <h3>A propos</h3>

        <p>AVSSI est une application produite par l'entreprise Bionactis International Group SA. Nos serveurs sont situées à Londres, et aux USA dans l'état de L'Oregon.</p>

        <p class="light">Contacter la société :</p>

        <ul>
          <li><i class="fa fa-envelope"></i> <a href="mailto:bionactis@romandie.com">bionactis@romandie.com</a></li>
          <li><i class="fa fa-phone"></i> +41 79 722 99 43</li>
          <li><i class="fa fa-map-marker"></i>1897 Le Bouveret Suisse</li>
        </ul>
      </div>

      <div class="une-colonne">
        <h3>Social</h3>

        <a href="#"><div class="un-social"><i class="fa fa-facebook"></i></div></a>

        <a href="#"><div class="un-social"><i class="fa fa-twitter"></i></div></a>

        <a href="#"><div class="un-social"><i class="fa fa-google-plus"></i></div></a>

        <a href="#"><div class="un-social"><i class="fa fa-linkedin"></i></div></a>

        <p class="light">Retrouvez la communauté d'AVSSI sur les réseaux sociaux. Nous faisons en sorte de rester le plus actif possible afin d'être à l'écoute de nos utilisateurs et de reccueillir leurs experiences ainsi que leur actualité.</p>
      </div>

      <div class="une-colonne">
        <h3>Localisation</h3>

        <div id="map"></div>
      </div>
    </div>
  </div>
</section>
