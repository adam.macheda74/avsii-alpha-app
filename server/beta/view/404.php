<section class="quatrecentquatre">
  <div class="center-block">
    <h1>Erreur 404</h1>
    <span>La page demandée est introuvable. Si le problème persiste, contactez un administrateur.</span>
  </div>
</section>
