<nav class="left">
  <header>
    <h1>Avssi</h1>
    <h2>Espace Membre</h2>
  </header>

  <ul>
    <li class="selected"><i class="fa fa-film"></i> Mes enregistrements</li>
    <li><i class="fa fa-lock"></i> Mot de passe</li>
    <li><i class="fa fa-shopping-basket"></i> Souscrire</li>
    <li><i class="fa fa-facebook-official"></i> Facebook</li>
  </ul>
</nav>

<nav class="right">
  <header class="top">
    <div class="flex-container">
      <h1>Enregistrements et informations</h1>
      <a href="<?php for($i = 0; $i<count($data['route'])-1; $i++) echo('../'); ?>service/deconnexion?redirect=true"><i class="fa fa-sign-out"></i></a>
    </div>
  </header>
  <header class="bottom">
    <div class="flex-container">
      <h1>Consulter la liste de vos preuves enregistrées ainsi que les informations de stockage.</h1>
      <a href="#">Demander de l'aide <i class="fa fa-question"></i></a>
    </div>
  </header>

  <section class="top-blocs flex-container">
    <div class="bloc-container">
      <div class="un-bloc">
        <div class="flex-container">
          <div class="progress-pie-chart" data-percent="<?php echo Utilisateur::getPourcentageStockage($data['listeVideos']); ?> ">
            <div class="ppc-progress">
              <div class="ppc-progress-fill"></div>
            </div>
            <div class="ppc-percents">
              <div class="pcc-percents-wrapper">
                <span>%</span>
              </div>
            </div>
          </div>

          <div class="desc-progress">
              <h1><?php echo round(Utilisateur::getStockageConsomme($data['listeVideos'])/60); ?></h1>
              <h2>minutes de stockage ont été consommées</h2>

              </br>

              <span>Il vous reste <?php echo Utilisateur::getStockageRestant($data['listeVideos']); ?> de stockage</span>
          </div>
        </div>
      </div>
    </div>

    <div class="bloc-container">
      <div class="un-autre-bloc">
        <div class="flex-container">
          <div class="progress-pie-chart2" data-percent="14">
            <div class="ppc-progress2">
              <div class="ppc-progress-fill2"></div>
            </div>
            <div class="ppc-percents2">
              <div class="pcc-percents-wrapper2">
                <span>%</span>
              </div>
            </div>
          </div>

          <div class="desc-progress">
              <h1>12</h1>
              <h2>jours restants avant expiration</h2>

              </br>

              <span>De votre offre premium</span>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="list-videos">
    <table>
      <tr>
        <th>Session</th><th>Date</th><th>Enregistrements</th><th>Durée totale</th>
      </tr>

      <?php
        foreach($data['listeVideos']['nbVideosByDate'] as $uneDate => $nb) {
          $tempstotal = 0;
          $miniature = null;
          foreach($data['listeVideos']['listeVideos'] as $uneVideo) {
            if($uneVideo[2] == $uneDate) {
              if($miniature == null)
                $miniature = $uneVideo[1];

              $tempstotal += Usual::timeToSeconds($uneVideo[4]);
            }
          }

          $video = "vidéos";

          if($nb == 1)
            $video = "vidéo";

          $video = $nb." ".$video;

          $tempstotal = gmdate("H:i:s", $tempstotal);

          echo "<tr>";
          echo "<td><img class='photo-miniature' src='../../uploads/$miniature'/></td><td>$uneDate</td><td>$video</td><td>$tempstotal</td>";
          echo"</tr>";
        }
      ?>
    </table>
  </section>
</nav>
