<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<?php if($data['viewMode'] != 'admin') { ?><meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"><?php } ?>
	<title>AVSSI</title>
	<link href="<?php for($i = 0; $i<count($data['route']); $i++) echo('../'); ?>res/css/font-awesome.min.css" rel="stylesheet">

	<?php if($data['viewMode'] != 'admin') { ?> <link rel="stylesheet" type="text/css" href="<?php for($i = 0; $i<count($data['route']); $i++) echo('../'); ?>res/css/style.css" /><?php }
	else { ?> <link rel="stylesheet" type="text/css" href="<?php for($i = 0; $i<count($data['route']); $i++) echo('../'); ?>/res/css/style-admin.css" />  <link rel="stylesheet" type="text/css" href="<?php for($i = 0; $i<count($data['route']); $i++) echo('../'); ?>res/css/progress.css" /><?php } ?>
	<script type="text/javascript">
		const SERV_URL = 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/beta/';
	</script>
</head>
<body>
	<?php if($data['viewMode'] != 'admin') { ?>

	<header>
		<div class="center-block">
			<section class="top-bar">
				<div class="flex-container">
					<div class="left">
						<h1>Avssi</h1> <span>Application de Vidéo Surveillance pour la Sécurité Individuelle</span>
					</div>
					<div class="right">
						<ul>
							<li><a href="<?php for($i = 0; $i<count($data['route'])-1; $i++) echo('../'); ?>page/accueil" class="selected">Accueil</a></li>
							<li><a href="<?php for($i = 0; $i<count($data['route'])-1; $i++) echo('../'); ?>page/membre">Accès membre</a></li>
						</ul>
					</div>
				</div>
			</section>
		</div>

		<div class="center-block">
			<div class="presentoir">
				<div class="description">
					<h1>Protégez</h1>
					<span>votre</span><h2>&nbsp;quotidien</h2>
					<p>Nous conservons toutes les preuves de vos videosurveillances en les sauvegardant en temps réel sur nos serveurs.</p>

					<a href="#" class="google-play-bouton"></a>
				</div>
			</div>
		</div>
	</header>

	<?php	} else {
		if(isset($data['page'])) { ?>

		<div id="bg-cover" onclick="hideVideo();"></div>

	  <div id="video-container">
			<video width="420" height="514" controls>
				<source src="http://ec2-34-209-142-173.us-west-2.compute.amazonaws.com/uploads/2_preuve_128.mp4" type="video/mp4">
				Votre navigateur ne supporte pas le lecteur de vidéo, essayez de le mettre à jour.
			</video>
	  </div>

		<div id="aide-container">
			<h1>Besoin d'aide ?</h1>
			<p>Si vous ne parvenez pas à utiliser l'espace membre en ligne ou que vous rencontrez des difficultés, vous pouvez contacter notre équipe responsable de la maintenance par <b>téléphone</b> ou par <b>mail</b>, via nos coordonnées :</p>
			</br></br>

			<h2><b>+33</b> 659 - 919 - 008</h2>
			<h3>contact@avssi.com</h3>
		</div>

		<nav class="left">
		  <header>
		    <h1>Avssi</h1>
		    <h2>Espace Membre</h2>
		  </header>

		  <ul>
		    <a href="<?php for($i = 0; $i<count($data['route'])-1; $i++) echo('../'); ?>page/membre/enregistrements"><li <?php if($data['page'] == 0) { ?>class="selected"<?php } ?>><i class="fa fa-film"></i> Mes enregistrements</li></a>
		    <a href="<?php for($i = 0; $i<count($data['route'])-1; $i++) echo('../'); ?>page/membre/mdp"><li <?php if($data['page'] == 1) { ?>class="selected"<?php } ?>><i class="fa fa-lock"></i> Mot de passe</li></a>
		    <a href="<?php for($i = 0; $i<count($data['route'])-1; $i++) echo('../'); ?>page/membre/offre"><li <?php if($data['page'] == 2) { ?>class="selected"<?php } ?>><i class="fa fa-shopping-basket"></i> Souscrire</li></a>
		    <a href="<?php for($i = 0; $i<count($data['route'])-1; $i++) echo('../'); ?>page/membre/facebook"><li <?php if($data['page'] == 3) { ?>class="selected"<?php } ?>><i class="fa fa-facebook-official"></i> Facebook</li></a>
		  </ul>
		</nav>

		<nav class="right">
		  <header class="top">
		    <div class="flex-container">
		      <h1><?php echo $data['titre']; ?></h1>
		      <a href="<?php for($i = 0; $i<count($data['route'])-1; $i++) echo('../'); ?>service/deconnexion?redirect=true"><i class="fa fa-sign-out"></i></a>
		    </div>
		  </header>
		  <header class="bottom">
		    <div class="flex-container">
		      <h1><?php echo $data['description']; ?></h1>
		      <a onclick="voirAide();" href="#">Demander de l'aide <i class="fa fa-question"></i></a>
		    </div>
		  </header>
	<?php }
		} ?>
