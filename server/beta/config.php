<?php
function loadMyClass($className) {
	if($className != 'Facebook\Facebook')
		include_once "model/".$className.".php";
}
spl_autoload_register('loadMyClass');

$_CONFIG['offre_defaut'] = 1; // Offre par défaut lors de l'inscription = Standard
$_CONFIG['offre_defaut_duree'] = 60; // Durée de l'offre défaut en minutes
