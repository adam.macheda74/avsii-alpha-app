function connexion() {
  if($.trim($('#mailInput').val()) == "" || $.trim($('#passInput').val()) == "") {
    Notif.erreur('Vous devez compléter tous les champs !');
  } else {
    $(".login-form .loader").css('opacity', 0.7);
    $("#mailInput").css('opacity', 0.7);
    $("#passInput").css('opacity', 0.7);

    $("#mailInput").attr('disabled','disabled');
    $("#passInput").attr('disabled','disabled');

    var mail = $.trim($('#mailInput').val());
    var pass = $.trim($('#passInput').val());

    setTimeout(function() {
      $.ajax({
        type: 'POST',
        url: SERV_URL+'service/connexion',
        data: {
          'mail' : mail,
          'pass' : pass
        },
        dataType: 'json',
        success: function (data) {
          if(data.result) {
            location.reload();
          } else {
            $(".login-form .loader").css('opacity', 0);
            $("#mailInput").css('opacity', 1);
            $("#passInput").css('opacity', 1);

            $("#mailInput").prop('disabled', false);
            $("#passInput").prop('disabled', false);

            Notif.erreur('Le mot de passe ou l\'adresse mail est incorrecte !');
          }
        }
      });
    }, 400);
  }
}

function mdpOublie() {
  $(".log-container").fadeOut(300);

  setTimeout(function() {
    $(".mdp-container").fadeIn(300);
  }, 300);
}

function backLogin() {
  $(".mdp-container").fadeOut(300);

  setTimeout(function() {
    $(".log-container").fadeIn(300);
  }, 300);
}
