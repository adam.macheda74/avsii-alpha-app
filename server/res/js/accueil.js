var slidePos = 1;

var slider = setInterval(function() {
  slidePos++;

  if(slidePos > 3) {
    slidePos = 1;
  }

  setSlide(slidePos);
}, 3000);

$("section.fonctionnement .phone-description").mouseenter(function() {
  clearInterval(slider);
});

$("section.fonctionnement .phone-description").mouseleave(function() {
  clearInterval(slider);
  slider = setInterval(function() {
    slidePos++;

    if(slidePos > 3) {
      slidePos = 1;
    }

    setSlide(slidePos);
  }, 3000);
});

function setSlide(n) {
  if(!$("section.fonctionnement .phone-description").is(":hover")) {
    clearInterval(slider);
    slidePos = n;

    slider = setInterval(function() {
      slidePos++;

      if(slidePos > 3) {
        slidePos = 1;
      }

      setSlide(slidePos);
    }, 3000);
  }

  if(n == 2) {
    $("section.fonctionnement .phone-viewer").css("opacity", 0);
    $("#bt-1 .plein").css('opacity', 0);
    $("#bt-2 .plein").css('opacity', 1);
    $("#bt-3 .plein").css('opacity', 0);

    $('#dec-1').css('animation', 'fadeOut 0.4s');
    $('#dec-3').css('animation', 'fadeOut 0.4s');

    setTimeout(function() {
      $("#dec-1").css('display', 'none');
      $("#dec-3").css('display', 'none');
      $("#dec-2").css('display', 'block');
      $('#dec-2').css('animation', 'fadeIn 0.4s');

      $("section.fonctionnement .phone-viewer").css("opacity", 1);
      $("section.fonctionnement .phone-viewer #img-1").css('transform', 'translate3d(0,-100%,0)');
      $("section.fonctionnement .phone-viewer #img-2").css('transform', 'translate3d(0,-100%,0)');
      $("section.fonctionnement .phone-viewer #img-3").css('transform', 'translate3d(0,-100%,0)');
    }, 200);
  } else if(n == 3) {
    $("section.fonctionnement .phone-viewer").css("opacity", 0);
    $("#bt-1 .plein").css('opacity', 0);
    $("#bt-2 .plein").css('opacity', 0);
    $("#bt-3 .plein").css('opacity', 1);

    $('#dec-1').css('animation', 'fadeOut 0.4s');
    $('#dec-2').css('animation', 'fadeOut 0.4s');

    setTimeout(function() {
      $("#dec-1").css('display', 'none');
      $("#dec-2").css('display', 'none');
      $("#dec-3").css('display', 'block');
      $('#dec-3').css('animation', 'fadeIn 0.4s');

      $("section.fonctionnement .phone-viewer").css("opacity", 1);
      $("section.fonctionnement .phone-viewer #img-1").css('transform', 'translate3d(0,-200%,0)');
      $("section.fonctionnement .phone-viewer #img-2").css('transform', 'translate3d(0,-200%,0)');
      $("section.fonctionnement .phone-viewer #img-3").css('transform', 'translate3d(0,-200%,0)');
    }, 200);
  } else {
    $("section.fonctionnement .phone-viewer").css("opacity", 0);
    $("#bt-1 .plein").css('opacity', 1);
    $("#bt-2 .plein").css('opacity', 0);
    $("#bt-3 .plein").css('opacity', 0);

    $('#dec-3').css('animation', 'fadeOut 0.4s');
    $('#dec-2').css('animation', 'fadeOut 0.4s');

    setTimeout(function() {
      $("#dec-3").css('display', 'none');
      $("#dec-2").css('display', 'none');
      $("#dec-1").css('display', 'block');
      $('#dec-1').css('animation', 'fadeIn 0.4s');

      $("section.fonctionnement .phone-viewer").css("opacity", 1);
      $("section.fonctionnement .phone-viewer #img-1").css('transform', 'translate3d(0,0,0)');
      $("section.fonctionnement .phone-viewer #img-2").css('transform', 'translate3d(0,0,0)');
      $("section.fonctionnement .phone-viewer #img-3").css('transform', 'translate3d(0,0,0)');
    }, 200);
  }
}

function initMap() {
  var uluru = {lat: 46.3792271, lng: 6.8637769};
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 9,
    center: uluru,
    styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-100},{"lightness":20}]},{"featureType":"road","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-100},{"lightness":40}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-10},{"lightness":30}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":-60},{"lightness":10}]},{"featureType":"landscape.natural","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":-60},{"lightness":60}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"},{"saturation":-100},{"lightness":60}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"},{"saturation":-100},{"lightness":60}]}]
  });
  var marker = new google.maps.Marker({
    position: uluru,
    map: map
  });
}
