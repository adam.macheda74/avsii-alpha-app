$(function(){
  var $ppc = $('.progress-pie-chart'),
    percent = parseInt($ppc.data('percent')),
    deg = 360*percent/100;
  if (percent > 50) {
    $ppc.addClass('gt-50');
  }
  $('.ppc-progress-fill').css('transform','rotate('+ deg +'deg)');
  $('.ppc-percents span').html(percent+'%');
});


$(function(){
  var $ppc = $('.progress-pie-chart2'),
    percent = parseInt($ppc.data('percent')),
    deg = 360*percent/100;
  if (percent > 50) {
    $ppc.addClass('gt-502');
  }
  $('.ppc-progress-fill2').css('transform','rotate('+ deg +'deg)');
  $('.ppc-percents2 span').html(percent+'%');
});

function voirVideo(url) {
  var video = $("div#video-container video")[0];
  video.src = SERV_URL+'../uploads/'+url;
  video.load();

  console.log(SERV_URL+'uploads/'+url);

  $('#bg-cover').fadeIn(400);
  $('#video-container').slideDown(400);
}

function voirAide(url) {
  $('#bg-cover').fadeIn(400);
  $('#aide-container').slideDown(400);
}

function hideVideo() {
  $('#bg-cover').fadeOut(400);
  if($('#video-container').css('display') == "block")
    $('#video-container').slideToggle(400);
  else if($('#aide-container').css('display') == "block")
    $('#aide-container').slideToggle(400);
}

$(document).keyup(function(e) {
 if (e.keyCode == 27) {
   if($('#bg-cover').css('display') == "block")
      hideVideo();
  }
});

function changeMdp() {
  var preerror = false;

  if($("#newpass-input").val() != $("#confirm-newpass-input").val()) {
    preerror = true;
    Notif.alerte('Les mots de passe doivent \352tre identiques !');
  } else if($.trim($('#newpass-input').val()).length < 6) {
    preerror = true;
    Notif.alerte('Le nouveau mot de passe doit faire au moins 6 caract\350res !');
  }

  if(!preerror) {
    $('nav.right section.mdp input[type=submit]').val('');
    $('nav.right section.mdp input[type=submit]').css("background", "url('"+SERV_URL+"../res/img/rolling.svg') 50% 50% / 18px no-repeat #626e7e");

    $.ajax({
      type: 'POST',
      url: SERV_URL+'service/mdp',
      data: {
        newpass: $("#newpass-input").val(),
        oldpass: $("#oldpass-input").val(),
      },
      dataType: 'json',
      success: function (data) {
        $('nav.right section.mdp input[type=submit]').val('Procéder au changement');
        $('nav.right section.mdp input[type=submit]').css("background", "#626e7e");

        if(data.result) {
          $("#confirm-newpass-input").val('');
          $("#oldpass-input").val('');
          $("#newpass-input").val('');

          Notif.succes('Le mot de passe a bien \351t\351 modifi\351.');
        } else {
          Notif.erreur('Votre ancien mot de passe est incorrect !');
        }
      }
    });
  }
}
