# InstantVideo

A Phonegap/Cordova plugin for Android to instant shot videos without controls like start and stop buttons.


To install :

```
cordova plugin add https://gitlab.com/adam.macheda74/cordova-macheda-instantvideo/
```

Example of code splitting video flux

``` javascript
function record() {
  cordova.plugins.instantvideo.start('yourfilename', 'back', true, null, null);

  setTimeout(function() {
    cordova.plugins.instantvideo.stop(record, null);
  }, 7000); // Files of 7 seconds
}

// Function called on click
function captureVideo() {
    record();
}
```

-- Based on jamesla backgroundvideo plugin