var cordova = require('cordova');

var instantvideo = {
    start : function(filename, camera, shouldRecordAudio, successFunction, errorFunction) {
        camera = camera || 'back';
        cordova.exec(successFunction, errorFunction, "instantvideo","start", [filename, camera, shouldRecordAudio]);
    },
    stop : function(successFunction, errorFunction) {
        cordova.exec(successFunction, errorFunction, "instantvideo","stop", []);
    }
};

module.exports = instantvideo;
