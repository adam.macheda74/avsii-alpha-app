function changerMdp() {
  var preerror = false;

  if($("#newpass-input").val() != $("#confirm-newpass-input").val()) {
    preerror = true;
    navigator.notification.alert('Les mots de passe doivent \352tre identiques !', null, 'Oups !', 'Terminer');
  } else if($.trim($('#newpass-input').val()).length < 6) {
    preerror = true;
    navigator.notification.alert('Le nouveau mot de passe doit faire au moins 6 caract\350res !', null, 'Oups !', 'Terminer');
  }

  if(!preerror) {
    $("section#change-mdp-page .list-view-loader").css("display", "block");

    $.ajax({
      type: 'POST',
      url: serveurURL+'/service/mdp',
      data: {
        newpass: $("#newpass-input").val(),
        oldpass: $("#oldpass-input").val(),
      },
      dataType: 'json',
      success: function (data) {
        $("section#change-mdp-page .list-view-loader").css("display", "none");

        if(data.result) {
          navigator.notification.alert('Mot de passe a bien \351t\351 modifi\351.', null, 'Modification r\351ussie', 'Terminer');
        } else {
          navigator.notification.alert('Votre ancien mot de passe est incorrect !', null, 'Oups !', 'Terminer');
        }
      }
    });
  }
}
