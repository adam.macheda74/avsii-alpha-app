  var videosEnCoursDeTransfert = new Array();
  var recordInterval;
  var timeRecorded = 0;
  var timeInterval;
  var erreurEnCours = false;
  var erreurLancee = false;
  var lockedConnexionTentative = false;

  function captureSuccess(mediaFile) {
      //console.log(mediaFile);
      uploadFile(mediaFile);
      if(recording) {
        record();
      }
  }

  function captureError(error) {
      var msg = 'An error occurred during capture: ' + error.code;
      //navigator.notification.alert(msg, null, 'Uh oh!');
  }

  function record() {
    recording = true;
    cordova.plugins.instantvideo.start(defaultVideoFileName, defaultCameraView, true, null, captureError);
  }

  function captureVideo() {
    if(!recording) {
      //drawProgressCircle(0, 0);
      lockedConnexionTentative = false;

      $('section button.start').html('Arr&ecirc;ter');
      $('section .ppc-record').css('animation', 'fade 1.3s infinite');
      $('section .recording').css('animation', 'fade 1.3s infinite');
      $('section .ppc-percents .under').html(toHHMMSS(timeRecorded));
      $('section .ppc-percents .under-under').html('Donn&eacute;es sauvegard&eacute;es');

      timeInterval = setInterval(function() {
        timeRecorded++;
        $('section .ppc-percents .under').html(toHHMMSS(timeRecorded));
      }, 1000);

      setTimeout(function() {
        if(STORAGE.getItem("emergency-mode") != null) {
          if(STORAGE.getItem("emergency-mode") == "true") {
            if (annyang) {
              window.system.setSystemVolume(0);
              annyang.start();
            }
          }
        } else {
          if (annyang) {
            window.system.setSystemVolume(0);
            annyang.start();
          }
        }
        record();

        recordInterval = setInterval(function() {
          cordova.plugins.instantvideo.stop(captureSuccess, captureError);
        }, splitSize*1000);
      }, 1);
    } else {
      if(STORAGE.getItem("emergency-mode") != null) {
        if(STORAGE.getItem("emergency-mode") == "true") {
          if (annyang) {
            annyang.abort();
            window.system.setSystemVolume(0.75);

            if(STORAGE.getItem("emergency-mode-callNb") != null)
              window.plugins.CallNumber.callNumber(function() { console.log('Call success');}, function() { console.log('Error when calling number');}, STORAGE.getItem("emergency-mode-callNb"), true);
            else
              window.plugins.CallNumber.callNumber(function() { console.log('Call success');}, function() { console.log('Error when calling number');}, emergencyNumDefault, true);
          }
        }
      } else {
        if (annyang) {
          annyang.abort();
          window.system.setSystemVolume(0.75);

          if(STORAGE.getItem("emergency-mode-callNb") != null)
            window.plugins.CallNumber.callNumber(function() { console.log('Call success');}, function() { console.log('Error when calling number');}, STORAGE.getItem("emergency-mode-callNb"), true);
          else
            window.plugins.CallNumber.callNumber(function() { console.log('Call success');}, function() { console.log('Error when calling number');}, emergencyNumDefault, true);
        }
      }

      recording = false;
      clearInterval(timeInterval);
      timeRecorded = 0;
      $('section .ppc-record').css('animation', 'none');
      $('section .recording').css('animation', 'none');
      $('section button.start').html('D&eacute;marrer surveillance');

      clearInterval(recordInterval);
      cordova.plugins.instantvideo.stop(captureSuccess, captureError);

      $('section .ppc-percents .under').html('Termin&eacute;');
    }
  }

  function clearVideosEnCours() {
    for(var i=0; i<videosEnCoursDeTransfert.length; i++) {
      if(videosEnCoursDeTransfert[i][2] == 100) {
        delete videosEnCoursDeTransfert[i];
        videosEnCoursDeTransfert = clearArray(videosEnCoursDeTransfert);
      }
    }
  }

  function uploadFile(mediaFile) {
      //navigator.notification.alert("Success recording file to " + mediaFile, null, 'Yeaa!');
      fileURL = mediaFile;

      //var uri = encodeURI("http://192.168.0.14/Stage/upload.php");
      var uri = encodeURI(serveurURI);
      var options = new FileUploadOptions();

      options.fileKey = "file";
      options.fileName = fileURL.substr(fileURL.lastIndexOf('/')+1);
      options.mimeType = "text/plain";

      var headers = {'headerParam':'headerValue'};
      options.headers = headers;

      var ft = new FileTransfer();

      videosEnCoursDeTransfert.push(new Array(ft._id, fileURL, 0));

      ft.onprogress = function(progressEvent) {
          if (progressEvent.lengthComputable) {
            clearVideosEnCours();

            for(var i=0; i<videosEnCoursDeTransfert.length; i++) {
             if(videosEnCoursDeTransfert[i][0] == ft._id) {
               videosEnCoursDeTransfert[i][2] = Math.ceil((progressEvent.loaded / progressEvent.total)*100);
             }
            }

            var nbVideos = videosEnCoursDeTransfert.length;
            var totalPourcentage = 0;

            for(var i=0; i<videosEnCoursDeTransfert.length; i++) {
              totalPourcentage += videosEnCoursDeTransfert[i][2];
            }

            erreurEnCours = false;

            drawProgressCircle(Math.ceil(totalPourcentage/nbVideos), nbVideos);
         }
      };

      ft.upload(fileURL, uri, onSuccess, onError, options);

      function onSuccess(r) {
        if(videosEnCoursDeTransfert.length == 0) {
          drawProgressCircle(100, 0);
        }
      }

      function onError(error) {
        for(var i=0; i<videosEnCoursDeTransfert.length; i++) {
          if(videosEnCoursDeTransfert[i][1] == error.source) {
            if(!erreurEnCours) {
              setTimeout(function() {
                recording = false;
                clearInterval(timeInterval);
                erreurEnCours = false;
                timeRecorded = 0;
                lockedConnexionTentative = true;
                $('section .ppc-record').css('animation', 'none');
                $('section .recording').css('animation', 'none');
                $('section button.start').html('D&eacute;marrer surveillance');

                clearInterval(recordInterval);
                videosEnCoursDeTransfert = new Array();
                cordova.plugins.instantvideo.stop(null, captureError);

                drawProgressCircle(0, 0);
                $('section .ppc-percents .under').html('Pas de video');
                $('section .ppc-percents .under-under').html('&agrave; transf&eacute;rer au serveur ');

                delete videosEnCoursDeTransfert[i];
                videosEnCoursDeTransfert = clearArray(videosEnCoursDeTransfert);

                navigator.notification.alert('Votre connexion internet n\'est pas suffisante pour continuer la surveillance !', null, 'Oups !', 'Terminer');
              }, tempsPerteConnexionBlocage*1000);
            }

            erreurEnCours = true;

            delete videosEnCoursDeTransfert[i];
            videosEnCoursDeTransfert = clearArray(videosEnCoursDeTransfert);

            if(!lockedConnexionTentative) uploadFile(error.source);

            $('section .ppc-percents .under-under').html('Perte de connexion au serveur');
            console.log('Tentative');
            /*console.log('Tentative de renvoi en cours !' + videosEnCoursDeTransfert);
            console.log(videosEnCoursDeTransfert.length);*/
          }
        }
      }
  }
