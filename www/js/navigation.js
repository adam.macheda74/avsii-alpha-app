var currentView = "login-page";
var currentLoginInputs = "login";
var loginAnimationEnCours = false;
var splashAnimationEnCours = false;
var keyBoardOpen = false;
var noQuitForm = false;

$('section button.start').bind('touchstart', function(){
    $('section button.start').css('background', '#5D6B7D');
}).bind('touchend', function(){
    $('section button.start').css('background', '#626e7e');
    captureVideo();
});

$('section .add-button').bind('touchstart', function(){
    $('section .add-button').css('opacity', '0.75');
}).bind('touchend', function(){
    $('section .add-button').css('opacity', '1');
    currentView = "record-page";
    $('section#record-page').css('transform', 'translate3d(0,0,0)');
});

$('#fbLoginBouton').bind('touchstart', function(){
    $('#fbLoginBouton').css('opacity', '0.8');
}).bind('touchend', function(){
    $('#fbLoginBouton').css('opacity', '1');
    loginFb();
});

$("#active-emergency-mode-button").bind('touchend', function() {
  if(STORAGE.getItem("emergency-mode") != null && STORAGE.getItem("emergency-mode") == "true") {
    $("#active-emergency-mode-button .checkbox").html(' ');
    STORAGE.setItem("emergency-mode", "false");
  } else if (STORAGE.getItem("emergency-mode") != null && STORAGE.getItem("emergency-mode") == "false") {
    $("#active-emergency-mode-button .checkbox").html('&#10003;');
    STORAGE.setItem("emergency-mode", "true");
  } else {
    $("#active-emergency-mode-button .checkbox").html(' ');
    STORAGE.setItem("emergency-mode", "false");
  }
});

$("#config-emergency-mode-button").bind('touchend', function() {
  if(dragging) {
    $(this).css('background', 'none');
  } else {
    $(this).css('background', 'none');
    configEmergencyMode();
  }
});

$('#left-tab-button').bind('touchstart', function(){
}).bind('touchend', function(){
  $('.menu-selector .left').removeClass('selected');
  $('.menu-selector .right').removeClass('selected');
  $('.menu-selector .left').addClass('selected');

  if(currentView == "config-list-page") {
    currentView = "record-list-page";
    $("#video-player-page").css("transform", "translate3d("+VW+"px, 0, 0)");
    $("#second-list-page").css("transform", "translate3d("+VW+"px, 0, 0)");
    $('section#record-page').css('transform', 'translate3d(0,'+VH+'px,0)');
    $('section#config-list-page').css('transform', 'translate3d('+VW+'px,0,0)');
    $('section#record-list-page').css('transform', 'translate3d(0,0,0)');
  } else if(currentView == "second-list-page") {
    currentView = "record-list-page";
    $('section#second-list-page').css('transform', 'translate3d('+VW+'px,0,0)');
    $('section#record-list-page').css('transform', 'translate3d(0,0,0)');
  } else if(currentView == "change-mdp-page") {
    // Correction bug graphique clavier
    $("#login-page").css('display', 'block');
    $("#record-page").css('display', 'block');

    currentView = "record-list-page";
    $('section#config-list-page').css('transform', 'translate3d('+VW+'px,0,0)');
    $('section#change-mdp-page').css('transform', 'translate3d('+VW+'px,0,0)');
    $('section#record-list-page').css('transform', 'translate3d(0,0,0)');
  } else if(currentView == "compte-fb-page") {
    // Correction bug graphique clavier
    $("#login-page").css('display', 'block');
    $("#record-page").css('display', 'block');

    currentView = "record-list-page";
    $('section#config-list-page').css('transform', 'translate3d('+VW+'px,0,0)');
    $('section#compte-fb-page').css('transform', 'translate3d('+VW+'px,0,0)');
    $('section#record-list-page').css('transform', 'translate3d(0,0,0)');
  } else if(currentView == "conso-page") {
    // Correction bug graphique clavier
    $("#login-page").css('display', 'block');
    $("#record-page").css('display', 'block');

    currentView = "record-list-page";
    $('section#config-list-page').css('transform', 'translate3d('+VW+'px,0,0)');
    $('section#conso-page').css('transform', 'translate3d('+VW+'px,0,0)');
    $('section#record-list-page').css('transform', 'translate3d(0,0,0)');
  }
});

$('#right-tab-button').bind('touchstart', function(){
}).bind('touchend', function(){
  $('.menu-selector .left').removeClass('selected');
  $('.menu-selector .right').removeClass('selected');
  $('.menu-selector .right').addClass('selected');

  if(currentView == "record-list-page") {
    currentView = "config-list-page";
    $('section#config-list-page').css('transform', 'translate3d(0,0,0)');
    $('section#record-list-page').css('transform', 'translate3d(-'+VW+'px,0,0)');
  } else if(currentView == "record-page") {
    currentView = "config-list-page";
    $("#video-player-page").css("transform", "translate3d("+VW+"px, 0, 0)");
    $("#second-list-page").css("transform", "translate3d("+VW+"px, 0, 0)");
    $('section#record-page').css('transform', 'translate3d(0,'+VH+'px,0)');
    $('section#config-list-page').css('transform', 'translate3d(0,0,0)');
    $('section#record-list-page').css('transform', 'translate3d(-'+VW+'px,0,0)');
  } else if(currentView == "second-list-page") {
    currentView = "config-list-page";
    $('section#second-list-page').css('transform', 'translate3d(-'+VW+'px,0,0)');
    $('section#config-list-page').css('transform', 'translate3d(0,0,0)');
  } else if(currentView == "video-player-page") {
    currentView = "config-list-page";
    $('section#video-player-page').css('transform', 'translate3d(-'+VW+'px,0,0)');
    $('section#config-list-page').css('transform', 'translate3d(0,0,0)');
  }
});

$('#connexionBouton').bind('touchstart', function(){
    $('#connexionBouton').css('background', '#5D6B7D');
}).bind('touchend', function(){
    connexion();
    $('#connexionBouton').css('background', '#626e7e');
});

$('#change-pass-submit-button').bind('touchstart', function(){
    $('#change-pass-submit-button').css('background', '#5D6B7D');
}).bind('touchend', function(){
    changerMdp();
    $('#change-pass-submit-button').css('background', '#626e7e');
});

$('#inscriptionBouton').bind('touchstart', function(){
    $('#inscriptionBouton').css('background-color', 'rgba(142, 216, 245, 0.75)');
}).bind('touchend', function(){
  if(currentLoginInputs == "register" && !loginAnimationEnCours) {
    inscription();
  }

  $('#inscriptionBouton').css('background-color', 'rgba(142, 216, 245, 0.85)');

  if(currentLoginInputs == "login" && !loginAnimationEnCours) {
    loginAnimationEnCours = true;
    currentLoginInputs = "register";
    noQuitForm = true;
    $('#connexionBouton').css('color', 'rgba(0, 0, 0, 0)');
    $('#connexionBouton').css('width', '0px');
    $('#connexionBouton').css('padding', '0px');

    $('#inscriptionBouton').css('width', '100%');
    $('#inscriptionBouton').css('color', 'white');
    $('#inscriptionBouton').css('padding-left', '35%');
    $('#inscriptionBouton').css('background-position', '25% 50%');

    setTimeout(function() {
      $('section#login-page .inputs input[type=password]#confirmPasswordInput').css('opacity', 1);
      $('section#login-page .titre h1').css('padding-top', '0vh');
      $('section#login-page .inputs').css('margin-top', '6vh');
      $('section#login-page .titre').css('margin-top', '-2vh');
      $('section#login-page .password-input-container').css('height', (Math.ceil($('section#login-page .password-input-container').height()*2))+'px')

      setTimeout(function() {
        loginAnimationEnCours = false;
      }, 700);
    }, 700);
  }
});

function hideRegisterForm() {
  if(currentLoginInputs == "register" && !loginAnimationEnCours) {
    loginAnimationEnCours = true;
    currentLoginInputs = "login";
    $('section#login-page .inputs input[type=password]#confirmPasswordInput').css('opacity', 0);

    $('section#login-page .password-input-container').css('height', (Math.ceil($('section#login-page .password-input-container').height()/2))+'px')

    $('section#login-page .titre h1').css('padding-top', '7vh');
    $('section#login-page .inputs').css('margin-top', '8vh');
    $('section#login-page .titre').css('margin-top', '0vh');

    setTimeout(function() {
      $('#connexionBouton').css('color', 'rgba(255, 255, 255, 1)');
      $('#connexionBouton').css('width', '78%');

      $('#inscriptionBouton').css('width', '20%');
      $('#inscriptionBouton').css('color', 'rgba(255, 255, 255, 0)');
      $('#inscriptionBouton').css('padding-left', '0');
      $('#inscriptionBouton').css('background-position', '50% 50%');

      setTimeout(function() {
        loginAnimationEnCours = false;
        noQuitForm = false;
      }, 700);
    }, 700);
  }
}

document.addEventListener("backbutton", onBackKeyDown, false);

function onBackKeyDown() {
  if(currentView == "record-page") {
    if(recording) {
      captureVideo();
    } else {
      $('section#record-page').css('transform', 'translate3d(0,'+VH+'px,0)');
      currentView = "record-list-page";
      setTimeout(loadVideosList(), 400);
    }
  } else if(currentView == "second-list-page") {
    $("#record-list-page").css("transform", "translate3d(0, 0, 0)");
    $("#second-list-page").css("transform", "translate3d("+VW+"px, 0, 0)");
    currentView = "record-list-page";
  } else if(currentView == "video-player-page") {
    $("section#video-player-page video")[0].pause();

    $("#second-list-page").css("transform", "translate3d(0, 0, 0)");
    $("#video-player-page").css("transform", "translate3d("+VW+"px, 0, 0)");
    currentView = "second-list-page";
  } else if(currentView == "login-page") {
    hideRegisterForm();

    if(!keyBoardOpen && !noQuitForm) {
      quitForm();
    }
  } else if(currentView == "change-mdp-page") {
    // Correction bug graphique clavier
    $("#login-page").css('display', 'block');
    $("#record-page").css('display', 'block');

    currentView = "config-list-page";
    $('section#config-list-page').css('transform', 'translate3d(0,0,0)');
    $('section#change-mdp-page').css('transform', 'translate3d('+VW+'px,0,0)');
  } else if(currentView == "compte-fb-page") {
    // Correction bug graphique clavier
    $("#login-page").css('display', 'block');
    $("#record-page").css('display', 'block');

    currentView = "config-list-page";
    $('section#config-list-page').css('transform', 'translate3d(0,0,0)');
    $('section#compte-fb-page').css('transform', 'translate3d('+VW+'px,0,0)');
  } else if(currentView == "conso-page") {
    // Correction bug graphique clavier
    $("#login-page").css('display', 'block');
    $("#record-page").css('display', 'block');

    currentView = "config-list-page";
    $('section#config-list-page').css('transform', 'translate3d(0,0,0)');
    $('section#conso-page').css('transform', 'translate3d('+VW+'px,0,0)');
  } else {
    if(!keyBoardOpen && !noQuitForm)
      quitForm();
  }
}

function quitForm() {
  navigator.notification.confirm(
      'Voulez-vous vraiment quitter l\'application ? Si une surveillance est en cours, elle sera automatiquement stopp\351e.',
       quitApp,
      'Quitter',
      ['Oui','Non']
  );
}

function quitApp(buttonIndex) {
  if(buttonIndex == 1) {
    navigator.app.exitApp();
  }
}

$("body").on("touchmove", function(){
  dragging = true;
});

$("body").on("touchstart", function(){
  dragging = false;
});

Hammer(document.querySelector("section#second-list-page .list-view")).on("swiperight", function(event) {
  $("#record-list-page").css("transform", "translate3d(0, 0, 0)");
  $("#second-list-page").css("transform", "translate3d("+VW+"px, 0, 0)");
  currentView = "record-list-page";

});

Hammer(document.querySelector("section#change-mdp-page .list-view")).on("swiperight", function(event) {
  // Correction bug graphique clavier
  $("#login-page").css('display', 'block');
  $("#record-page").css('display', 'block');

  $("#change-mdp-page").css("transform", "translate3d("+VW+"px, 0, 0)");
  $("#config-list-page").css("transform", "translate3d(0, 0, 0)");
  currentView = "config-list-page";

});

Hammer(document.querySelector("section#compte-fb-page .list-view")).on("swiperight", function(event) {
  // Correction bug graphique clavier
  $("#login-page").css('display', 'block');
  $("#record-page").css('display', 'block');

  $("#compte-fb-page").css("transform", "translate3d("+VW+"px, 0, 0)");
  $("#config-list-page").css("transform", "translate3d(0, 0, 0)");
  currentView = "config-list-page";

});

Hammer(document.querySelector("section#conso-page .list-view")).on("swiperight", function(event) {
  // Correction bug graphique clavier
  $("#login-page").css('display', 'block');
  $("#record-page").css('display', 'block');

  $("#conso-page").css("transform", "translate3d("+VW+"px, 0, 0)");
  $("#config-list-page").css("transform", "translate3d(0, 0, 0)");
  currentView = "config-list-page";

});

Hammer(document.getElementById('video-player-page')).on("swiperight", function(event) {
  $("section#video-player-page video")[0].pause();

  $("#second-list-page").css("transform", "translate3d(0, 0, 0)");
  $("#video-player-page").css("transform", "translate3d("+VW+"px, 0, 0)");
  currentView = "second-list-page";
});

function hideLoginPage() {
  $("#login-page").css("transform", "translate3d(0, "+VH+"px, 0)");
  currentView = "record-list-page";
}

function showLoginPage() {
  $("#login-page").css("transform", "translate3d(0, 0, 0)");
  currentView = "login-page";
}

$("section#config-list-page .list-view .un-elem").each(function(){
  $(this).bind('touchstart', function(e){
    $(this).css('background', '#232F3E');
  }).bind('touchend', function(){
    $(this).css('background', 'none');
  });
});

$("#logout-button").bind('touchend', function() {
  if(dragging) {
    $(this).css('background', 'none');
  } else {
    $(this).css('background', 'none');
    deconnexion();
  }
});

$("#change-pass-button").bind('touchend', function() {
  if(dragging) {
    $(this).css('background', 'none');
  } else {
    $(this).css('background', 'none');
    $("#config-list-page").css("transform", "translate3d(-"+VW+"px, 0, 0)");
    $("#change-mdp-page").css("transform", "translate3d(0, 0, 0)");
    currentView = "change-mdp-page";

    // Correction bug graphique clavier
    $("#login-page").css('display', 'none');
    $("#record-page").css('display', 'none');
  }
});

$("#compte-fb-button").bind('touchend', function() {
  if(dragging) {
    $(this).css('background', 'none');
  } else {
    $(this).css('background', 'none');
    $("#config-list-page").css("transform", "translate3d(-"+VW+"px, 0, 0)");
    $("#compte-fb-page").css("transform", "translate3d(0, 0, 0)");
    currentView = "compte-fb-page";

    // Correction bug graphique clavier
    $("#login-page").css('display', 'none');
    $("#record-page").css('display', 'none');
  }
});

$("#conso-button").bind('touchend', function() {
  if(dragging) {
    $(this).css('background', 'none');
  } else {
    $(this).css('background', 'none');
    $("#config-list-page").css("transform", "translate3d(-"+VW+"px, 0, 0)");
    $("#conso-page").css("transform", "translate3d(0, 0, 0)");
    currentView = "conso-page";

    // Correction bug graphique clavier
    $("#login-page").css('display', 'none');
    $("#record-page").css('display', 'none');
  }
});

$("#actualiser-button").bind('touchend', function() {
  if(dragging) {
    $(this).css('background', 'none');
  } else {
    $(this).css('background', 'none');
    $("#config-list-page").css("transform", "translate3d("+VW+"px, 0, 0)");
    $("#record-list-page").css("transform", "translate3d(0, 0, 0)");
    currentView = "record-list-page";

    $('.menu-selector .left').removeClass('selected');
    $('.menu-selector .right').removeClass('selected');
    $('.menu-selector .left').addClass('selected');
    loadVideosList();
  }
});

function showSpashScreen() {
  if(!splashAnimationEnCours) {
    splashAnimationEnCours = true;
    $("section#login-page .inputs").css("opacity", "0");
    $("section#login-page .submits").css("margin-top", "40vh");
    $("section#login-page .titre").css("margin-top", "8vh");

    setTimeout(function() {
      $('section#login-page .loading').css('display', 'block');
      splashAnimationEnCours = false;
    }, 600);
  }
}

function hideSpashScreen() {
  if(!splashAnimationEnCours) {
    splashAnimationEnCours = true;
    $('section#login-page .loading').css('opacity', '0');
    setTimeout(function() {
      $('section#login-page .loading').css('display', 'none');
      $("section#login-page .inputs").css("opacity", "1");
      $("section#login-page .submits").css("margin-top", "6vh");
      $("section#login-page .titre").css("margin-top", "0");
      $("section#login-page .inputs").css("margin-top", "8vh");
      splashAnimationEnCours = false;
    }, 400);
  }
}

// Gérer les débordements graphiques lors de la saisie du clavier (apparition keyboard)

window.addEventListener('native.keyboardshow', keyboardShowHandler);

function keyboardShowHandler(e) {
  keyBoardOpen = true;

  if(currentLoginInputs == "login") {
    $('section#login-page .titre h1').css('padding-top', '0vh');
    $('section#login-page .inputs').css('margin-top', '6vh');
    $('section#login-page .titre').css('margin-top', '-2vh');
  }
}

window.addEventListener('native.keyboardhide', keyboardHideHandler);

function keyboardHideHandler(e) {
  keyBoardOpen = false;

  if(currentLoginInputs == "login") {
    $('section#login-page .titre h1').css('padding-top', '7vh');
    $('section#login-page .inputs').css('margin-top', '8vh');
    $('section#login-page .titre').css('margin-top', '0vh');
  }
}
