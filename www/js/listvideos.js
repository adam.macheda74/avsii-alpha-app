  function loadVideosList() {
    checkMiniatures();
  }

  function checkMiniatures() {
    $("section#record-list-page .list-view-loader").css("display", "block");

    $.ajax({
      type: 'GET',
      url: serveurURL+'/video/liste',
      data: {},
      dataType: 'json',
      success: function (data) {
        //if(data['miniIndispo'].length == 0) {
          remplirListe(data);
          $("section#record-list-page .list-view-loader").css("display", "none");
        //}
      }
    });
  }

  function remplirListe(liste) {
    $("section#record-list-page .list-view .list-view-content").html("");
    var idLink = 0;

    $.each(liste['nbVideosByDate'], function(index, value) {
      var miniature = null;

      for(var i = 0; i<liste['listeVideos'].length; i++) {
        if(liste['listeVideos'][i][2] == index) {
          if(miniature == null) {
            miniature = liste['listeVideos'][i][1];
          }
        }
      }

      var vidTexte = "vid&eacute;o";
      if(value > 1 || value == 0) vidTexte = "vid&eacute;os";

      //<div class="control-buttons"><a href="#" class="see-button"></a><a href="#" class="delete-button"></a></div>

      $("section#record-list-page .list-view .list-view-content").append('<div class="un-elem" id="elem-'+idLink+'"><div class="flex-container"><div class="desc"><div class="img" style="background: url('+serveurURLGF+'/uploads/'+miniature+') center center no-repeat, url(\'./img/loader.svg\') 50% 50% / 32px no-repeat;"></div><span><b>'+index+'</b></br> '+value+' '+vidTexte+'</span></div></div></div>');


      $('#elem-'+idLink).bind('touchstart', function(){
        $(this).css('background', '#232F3E');
      }).bind('touchend', function(){
        if(dragging) {
          $(this).css('background', 'none');
        } else {
          $(this).css('background', 'none');
          afficherSuiteListe($(this).find('span b').text(), liste);
        }
      });

      idLink ++;
    });
  }

  function remplirSuiteListe(id, liste) {
    var getVideos = Array();

    for(var i = 0; i<liste['listeVideos'].length; i++) {
      if(liste['listeVideos'][i][2] == id) {
        if(liste['listeVideos'][i][4] != false) {
          getVideos.push(liste['listeVideos'][i]);
        }
      }
    }

    var vidTexte = "vid&eacute;o";
    if(getVideos.length > 1 || getVideos.length == 0) vidTexte = "vid&eacute;os";

    $('section#second-list-page .list-view .titre').html('Le '+id+' <u>'+getVideos.length+' '+vidTexte+'</u>');

    $("section#second-list-page .list-view .list-view-content").html("");

    for(var i = 0; i<getVideos.length; i++) {
      if(getVideos[i][4] != false) {
        $("section#second-list-page .list-view .list-view-content").append('<div class="un-elem" id="video-'+i+'"><div class="flex-container"><div class="desc"><div class="img" style="background: url('+serveurURLGF+'/uploads/'+getVideos[i][1]+') center center no-repeat, url(\'./img/loader.svg\') 50% 50% / 32px no-repeat;"></div><span><b>&Agrave; '+getVideos[i][3]+'</b></br> Dur&eacute;e '+getVideos[i][4]+'</span></div></div></div>');

        $('#video-'+i).bind('touchstart', function(e){
          $(this).css('background', '#232F3E');
        }).bind('touchend', function(){
          if(dragging) {
            $(this).css('background', 'none');
          } else {
            $(this).css('background', 'none');
            afficherVideo(getVideos[$(this).attr('id').substr(6)]);
          }
        });
      }
    }

    $('section#second-list-page .un-elem').each(function(){   //tagname based selector
      var mc = new Hammer(this);
      var $this = $(this);
      mc.on("press", function() {
        if(!dragging) {
          dragging = true;
          showDeleteButtons(id, liste);
        }
      });
    });
  }

  function showDeleteButtons(id, liste) {
    navigator.vibrate(100);

    $('section#second-list-page .un-elem').each(function() {
      $(this).find('.control-buttons').remove();
      $(this).find('.flex-container').append('<div class="control-buttons"><a href="#" id="del-vid-'+$(this).attr('id').substr(6)+'" class="delete-button"></a></div>');

      var mc = new Hammer(document.getElementById('del-vid-'+$(this).attr('id').substr(6)));
      var $this = $(this);
      mc.on("tap", function() {
        dragging = true;
        $this.find('.delete-button').css('-webkit-filter', 'grayscale(100%)');
        $this.slideUp(150);

        var getVideos = Array();

        for(var i = 0; i<liste['listeVideos'].length; i++) {
          if(liste['listeVideos'][i][2] == id) {
            getVideos.push(liste['listeVideos'][i]);
          }
        }

        deleteVideo(getVideos[$this.attr('id').substr(6)][0], liste, id);
      });
    });
  }

  function deleteVideo(nom, liste, idp) {
    console.log(nom);

    $.ajax({
      type: 'POST',
      url: serveurURL+'/video/delete',
      data: {
        'vid': nom
      },
      dataType: 'json',
      success: function (data) {
        if(!data.result) {
          navigator.notification.alert('Une erreur est survenue lors de la suppression de la video !', null, 'Oups !', 'Terminer');
        }
      }
    });

    for(var i = 0; i<liste['listeVideos'].length; i++) {
      if(liste['listeVideos'][i][0] == nom) {
        var dateVideoDeleted = liste['listeVideos'][i][2];
        delete liste['listeVideos'][i];
        liste['listeVideos'] = clearArray(liste['listeVideos']);
      }
    }

    $.each(liste['nbVideosByDate'], function(index, value) {
      // ------- Mise à jour du nb dans la liste
      var nb = 0;

      for(var j = 0; j<liste['listeVideos'].length; j++) {
        if(liste['listeVideos'][j][2] == index) {
          if(liste['listeVideos'][j][4] != false) {
            nb++;
            console.log(liste['listeVideos'][j][2]);
          }
        }
      }

      liste['nbVideosByDate'][index] = nb;

      if(liste['nbVideosByDate'][index] == 0) {
        delete liste['nbVideosByDate'][index];
        liste['nbVideosByDate'] = clearArray(liste['nbVideosByDate']);
      }
    });

    remplirListe(liste);

    // --- Reinitialisation titre

    var getVideos = Array();

    for(var i = 0; i<liste['listeVideos'].length; i++) {
      if(liste['listeVideos'][i][2] == idp) {
        if(liste['listeVideos'][i][4] != false) {
          getVideos.push(liste['listeVideos'][i]);
        }
      }
    }

    var vidTexte = "vid&eacute;o";
    if(getVideos.length > 1 || getVideos.length == 0) vidTexte = "vid&eacute;os";

    $('section#second-list-page .list-view .titre').html('Le '+idp+' <u>'+getVideos.length+' '+vidTexte+'</u>');

    if(getVideos.length == 0) {
      $("#record-list-page").css("transform", "translate3d(0, 0, 0)");
      $("#second-list-page").css("transform", "translate3d(100vw, 0, 0)");
      currentView = "record-list-page";
    }
  }

  function afficherSuiteListe(id, liste) {
    remplirSuiteListe(id, liste);
    $("#record-list-page").css("transform", "translate3d(-100vw, 0, 0)");
    $("#second-list-page").css("transform", "translate3d(0, 0, 0)");
    currentView = "second-list-page";
  }

  function afficherVideo(vid) {
    var video = $("section#video-player-page video")[0];
    video.src = serveurURLGF+'/uploads/'+vid[0];
    video.load();

    $("#second-list-page").css("transform", "translate3d(-100vw, 0, 0)");
    $("#video-player-page").css("transform", "translate3d(0, 0, 0)");
    currentView = "video-player-page";

    $('section#second-list-page .un-elem').each(function() {
      $(this).find('.control-buttons').remove();
    });
  }
