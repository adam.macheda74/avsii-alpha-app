// Fixation des hauteurs pour certains blocs en fonction de la hauteur du départ de l'écran

document.addEventListener("deviceready", onDeviceReady, false);

var VH;
var VW;

function onDeviceReady() {
  VH = $('section#login-page').height();
  VW = $('section#login-page').width();

  $('section#change-mdp-page input[type=password]').css('height', Math.ceil($('section#change-mdp-page input[type=password]').height()));
  $('section#change-mdp-page input[type=submit]').css('height', Math.ceil($('section#change-mdp-page input[type=submit]').height()));

  $('section#login-page .inputs input[type=text]').css('height', Math.ceil($('section#login-page .inputs input[type=text]').height()));
  $('section#login-page .inputs input[type=password]').css('height', Math.ceil($('section#login-page .inputs input[type=password]').height()));
  $('section#login-page .inputs input[type=submit]').css('height', Math.ceil($('section#login-page .inputs input[type=submit]').height()));
  $('section#login-page .password-input-container').css('height', Math.ceil(((VH*8)/100)+5));
  $('section#login-page .inputs input[type=submit]#inscriptionBouton').css('height', Math.ceil($('section#login-page .inputs input[type=submit]#inscriptionBouton').height()));
  $('section#login-page .inputs input[type=submit]#inscriptionBouton').css('background-size', Math.ceil($('section#login-page .inputs input[type=submit]#inscriptionBouton').height()*0.5+'px'));

  $("#video-player-page").css("transform", "translate3d("+VW+"px, 0, 0)");
  $("#second-list-page").css("transform", "translate3d("+VW+"px, 0, 0)");
  $('section#record-page').css('transform', 'translate3d(0,'+VH+'px,0)');
  $('section#config-list-page').css('transform', 'translate3d('+VW+'px,0,0)');
  $('section#record-list-page').css('transform', 'translate3d(0,0,0)');
  $('section#change-mdp-page').css('transform', 'translate3d('+VW+'px,0,0)');

  autoLogin();
  loadOptions();
}
