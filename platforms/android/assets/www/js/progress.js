var precPer = 0;

function drawProgressCircle(per, nVid, done) {
  if (typeof(done)==='undefined') done = false;

  $(function(){
    if(isNaN(per)) per = 100;

    var $ppc = $('.progress-pie-chart'),
      deg = 360*per/100;

    if(precPer > per) {
      $ppc.removeClass('gt-50');
    }

    if (per > 50) {
      $ppc.addClass('gt-50');
    }

    $('.ppc-progress-fill').css('transform','rotate('+ deg +'deg)');
    $('.ppc-percents span').html(per+'<b>%</b>');

    if(per < 100) {
      $('section .ppc-percents .under-under').html('Sauvegarde sur le serveur');
    } else {
      $('section .ppc-percents .under-under').html('Donn&eacute;es sauvegard&eacute;es');
    }

    precPer = per;
  });
}
