var Notif = {
  alerte: function(txt) {
    if($('#centre-notifications').hasClass('erreur'))
      $('#centre-notifications').removeClass('erreur');
    else if($('#centre-notifications').hasClass('succes'))
      $('#centre-notifications').removeClass('succes');
    else if($('#centre-notifications').hasClass('alerte'))
      $('#centre-notifications').removeClass('alerte');

    $('#centre-notifications').addClass('alerte')
    $('#centre-notifications').html(txt);
    $('#centre-notifications').css('margin-top', '0');

    setTimeout(function() {
      $('#centre-notifications').css('margin-top', '-6vh');
    }, 2000)
  },

  succes: function(txt) {
    if($('#centre-notifications').hasClass('erreur'))
      $('#centre-notifications').removeClass('erreur');
    else if($('#centre-notifications').hasClass('succes'))
      $('#centre-notifications').removeClass('succes');
    else if($('#centre-notifications').hasClass('alerte'))
      $('#centre-notifications').removeClass('alerte');

    $('#centre-notifications').addClass('succes')
    $('#centre-notifications').html(txt);
    $('#centre-notifications').css('margin-top', '0');

    setTimeout(function() {
      $('#centre-notifications').css('margin-top', '-6vh');
    }, 2000)
  },

  erreur: function(txt) {
    if($('#centre-notifications').hasClass('erreur'))
      $('#centre-notifications').removeClass('erreur');
    else if($('#centre-notifications').hasClass('succes'))
      $('#centre-notifications').removeClass('succes');
    else if($('#centre-notifications').hasClass('alerte'))
      $('#centre-notifications').removeClass('alerte');

    $('#centre-notifications').addClass('erreur')
    $('#centre-notifications').html(txt);
    $('#centre-notifications').css('margin-top', '0');

    setTimeout(function() {
      $('#centre-notifications').css('margin-top', '-6vh');
    }, 2000)
  }
};
