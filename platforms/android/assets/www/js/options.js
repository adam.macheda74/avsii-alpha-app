function loadOptions() {
  if(STORAGE.getItem("emergency-mode") != null && STORAGE.getItem("emergency-mode") == "true") {
    $("#active-emergency-mode-button .checkbox").html('&#10003;');
  } else if (STORAGE.getItem("emergency-mode") != null && STORAGE.getItem("emergency-mode") == "false") {
    $("#active-emergency-mode-button .checkbox").html(' ');
  } else {
    $("#active-emergency-mode-button .checkbox").html('&#10003;');
    STORAGE.setItem("emergency-mode", "true");
  }
}

function saveConfigEmergency(re) {
  if(re.buttonIndex == 1) {
    var phone_pattern = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    if(phone_pattern.test(re.input1)) {
      STORAGE.setItem("emergency-mode-callNb", re.input1);
    } else {
      var reg = /^\d+$/;
      if(reg.test(re.input1) && re.input1.length >= 2 && re.input1.length <= 12) {
        STORAGE.setItem("emergency-mode-callNb", re.input1);
      } else {
        navigator.notification.alert('Le num\351ro entr\351 est incorrect.', null, 'Oups !', 'J\'ai compris');
      }
    }
  }
}

function configEmergencyMode() {
  if(!(STORAGE.getItem("emergency-mode-callNb") != null))
    STORAGE.setItem("emergency-mode-callNb", emergencyNumDefault);

  navigator.notification.prompt(
    'Num\351ro contact\351 automatiquement lorsque le mot cl\351 "help" est prononc\351 pendant l\'enregistrement :',  // message
    saveConfigEmergency,                  // callback to invoke
    'Configurer mode URGENCE',            // title
    ['Sauver','Annuler'],             // buttonLabels
    STORAGE.getItem("emergency-mode-callNb")                // defaultText
  );
}
