document.addEventListener('deviceready', function() {
  const permissions = cordova.plugins.permissions;

  permissions.checkPermission(permissions.WRITE_EXTERNAL_STORAGE, function() {
    console.log('Ok permissions');
  }, function() {
    permissions.requestPermission(permissions.WRITE_EXTERNAL_STORAGE, null, null);
  });


  if (annyang) {
    // Let's define our first command. First the text we expect, and then the function it should call
    var commands = {
      'help': function() {
        captureVideo();
        annyang.abort();
        navigator.notification.alert('Mode URGENCE activ\351, nous allons contacter le num\351ro d\'urgence que vous avez configur\351.', null, 'ALERTE', 'Ok');
      }
    };

    annyang.setLanguage('fr-FR');

    // Add our commands to annyang
    annyang.addCommands(commands);

    annyang.addCallback('resultMatch', function(userSaid, commandText, phrases) {
      console.log(userSaid); // sample output: 'hello'
      console.log(commandText); // sample output: 'hello (there)'
      console.log(phrases); // sample output: ['hello', 'halo', 'yellow', 'polo', 'hello kitty']
    });

    annyang.addCallback('resultNoMatch', function(phrases) {
      console.log("I think the user said: ", phrases[0]);
      console.log("But then again, it could be any of the following: ", phrases);
    });


  }

 });
