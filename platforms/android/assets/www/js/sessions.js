
function connexion(mail, pass) {
  if (typeof(mail)==='undefined') mail = null;
  if (typeof(pass)==='undefined') pass = null;

  var errors = false;

  if(mail == null) {
    if($.trim($("#mailInput").val()) != '') {
      mail = $.trim($("#mailInput").val());
    } else {
      errors = true;
    }
  }

  if(pass == null) {
    if($.trim($("#passwordInput").val()) != '') {
      pass = $("#passwordInput").val();
    } else {
      errors = true;
    }
  }

  if(errors) {
    Notif.erreur('Tous les champs doivent &ecirc;tre compl&eacute;t&eacute;s !');
  } else {
    if(arguments.callee.caller.name.toString() != 'autoLogin')
      $('.full-loader').css('display','block');

    $.ajax({
      type: 'POST',
      url: serveurURL+'/service/connexion',
      data: {
        'mail' : mail,
        'pass' : pass
      },
      dataType: 'json',
      success: function (data) {
        $('.full-loader').css('display','none');
        if(data.result) {

          var testHideSplash = setInterval(function() {
            if(!splashAnimationEnCours) {
              setTimeout(function() {
                clearInterval(testHideSplash);
                loadVideosList();
                hideLoginPage();

                STORAGE.setItem("loggedOut", "false");
                hideSpashScreen();
              }, 400);
            }
          }, 100);
        } else {
          var testHideSplash = setInterval(function() {
            if(!splashAnimationEnCours) {
              setTimeout(function() {
                clearInterval(testHideSplash);
                hideSpashScreen();
                Notif.erreur('Le mot de passe ou l\'adresse mail est incorrecte !');
              }, 400);
            }
          }, 100);
        }
      }
    });
  }
}

function inscription() {
  if($.trim($("#mailInput").val()) == '' || $.trim($("#passwordInput").val()) == '' || $.trim($("#confirmPasswordInput").val()) == '') {
    Notif.erreur('Tous les champs doivent &ecirc;tre compl&eacute;t&eacute;s !');
  } else {
    if($('#passwordInput').val() != $('#confirmPasswordInput').val()) {
      Notif.erreur('Les deux mots de passe ne correspondent pas !');
    } else {
      if($.trim($('#passwordInput').val()).length < 6) {
        Notif.erreur('Le mot de passe doit faire au moins 6 caract&egrave;res.');
      } else {
        $('.full-loader').css('display','block');

        $.ajax({
          type: 'POST',
          url: serveurURL+'/service/inscription',
          data: {
            'mail' : $.trim($("#mailInput").val()),
            'pass' : $("#passwordInput").val(),
            'confirm' : $("#confirmPasswordInput").val()
          },
          dataType: 'json',
          success: function (data) {
            $('.full-loader').css('display','none');

            if(data.result) {
              hideRegisterForm();
              loadVideosList();
              hideLoginPage();
            } else {
              Notif.erreur(data.error);
            }
          }
        });
      }
    }
  }
}

function deconnexion() {
  $('.full-loader').css('display','block');

  $.ajax({
    type: 'POST',
    url: serveurURL+'/service/deconnexion',
    data: {},
    dataType: 'json',
    success: function (data) {
      $('.full-loader').css('display','none');

      if(data.result) {
        STORAGE.setItem("loggedOut", "true");

        showLoginPage();

        setTimeout(function() {
          currentView = "login-page";
          $("#video-player-page").css("transform", "translate3d("+VW+"px, 0, 0)");
          $("#second-list-page").css("transform", "translate3d("+VW+"px, 0, 0)");
          $('section#record-page').css('transform', 'translate3d(0,'+VH+'px,0)');
          $('section#config-list-page').css('transform', 'translate3d('+VW+'px,0,0)');
          $('section#record-list-page').css('transform', 'translate3d(0,0,0)');
          $('section#change-mdp-page').css('transform', 'translate3d('+VW+'px,0,0)');
        }, 700);
      } else {
        navigator.notification.alert('Une erreur est survenue lors de la d\351connexion !', null, 'Oups !', 'Terminer');
      }
    }
  });
}

function autoLogin() {
  //showSpashScreen();

  if(lsTest() === true) {
    if(STORAGE.getItem("loggedOut") != null && STORAGE.getItem("loggedOut") == "true") {
      var testHideSplash = setInterval(function() {
        if(!splashAnimationEnCours) {
          setTimeout(function() {
            clearInterval(testHideSplash);
            hideSpashScreen();
          }, 400);
        }
      }, 100);
    } else {
      if(!getSession()) {
        if(STORAGE.getItem("mailUtilisateur") != null && STORAGE.getItem("passUtilisateur") != null) {
          connexion(STORAGE.getItem("mailUtilisateur"), STORAGE.getItem("passUtilisateur"));
        } else {
          var testHideSplash = setInterval(function() {
            if(!splashAnimationEnCours) {
              setTimeout(function() {
                clearInterval(testHideSplash);
                hideSpashScreen();
              }, 400);
            }
          }, 100);
        }
      } else {
        var testHideSplash = setInterval(function() {
          if(!splashAnimationEnCours) {
            setTimeout(function() {
              clearInterval(testHideSplash);
              loadVideosList();
              hideLoginPage();

              hideSpashScreen();
            }, 400);
          }
        }, 100);
      }
    }
  } else {
    var testHideSplash = setInterval(function() {
      if(!splashAnimationEnCours) {
        setTimeout(function() {
          clearInterval(testHideSplash);
          hideSpashScreen();
        }, 400);
      }
    }, 100);
  }
}

function getSession() {
  var res;

  $.ajax({
    type: 'POST',
    url: serveurURL+'/service/session',
    data: {},

    dataType: 'json',
    success: function (data) {
      if(data.statut) {
        res = data;
      } else {
        res = false;
      }
    }
  });

  return res;
}
